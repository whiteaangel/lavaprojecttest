﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private BulletSettings settings;
    private Vector3 _vel;
    private bool hit = false;
    private void Start()
    {
        StartCoroutine(Destroy());
    }

    private void OnTriggerEnter(Collider other)
    {
        if(hit)
            return;
        var rb = other.GetComponent<Rigidbody>();
        if(rb == null)
            return;
        hit = true;
        rb.velocity = settings.impactStrength;
        Destroy(gameObject);
    }

    private void Update()
    {
        if (!Physics.Raycast(this.transform.position, Vector3.forward, out var hit, 30)) return;
        if (!hit.collider.CompareTag("Enemy"))
            return;
        hit.collider.GetComponent<RagdollEnemyController>().AddRagdoll();
    }

    IEnumerator Destroy()
    {
        yield return new WaitForSeconds(5f);
        Destroy(gameObject);
    }
}

