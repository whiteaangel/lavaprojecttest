﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerSpeed", menuName = "ScriptableObject/PlayerSpeed")]
public class PlayerSpeed : ScriptableObject
{
   public float speed;
}
