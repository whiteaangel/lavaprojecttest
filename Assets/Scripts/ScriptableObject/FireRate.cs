﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FireRate", menuName = "ScriptableObject/FireRate")]
public class FireRate : ScriptableObject
{
  public float fireRate;
}
