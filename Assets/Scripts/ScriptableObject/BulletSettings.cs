﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BulletSettings", menuName = "ScriptableObject/BulletSettings")]
public class BulletSettings : ScriptableObject
{
   public Vector3 impactStrength = new Vector3();
}
