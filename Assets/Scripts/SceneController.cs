﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    [SerializeField] private CanvasGroup hintCanvasGroup;
    public void Start()
    {
        if (PlayerPrefs.GetInt("Hint") != 0) return;
        StartCoroutine(HideHint());
        hintCanvasGroup.alpha = 1;
        PlayerPrefs.SetInt("Hint",1);
        PlayerPrefs.Save();
    }

    IEnumerator HideHint()
    {
        yield return new WaitForSeconds(4f);
        hintCanvasGroup.alpha = 0;
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
