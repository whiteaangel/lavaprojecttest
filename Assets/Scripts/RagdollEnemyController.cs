﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollEnemyController : MonoBehaviour
{
    [SerializeField] private Collider collider;
    [SerializeField] private Animator animator;
    private Rigidbody[] _rigidbodies;
    private Collider[] _colliders;

    private void Awake()
    {
        _rigidbodies = GetComponentsInChildren<Rigidbody>();
        _colliders = GetComponentsInChildren<Collider>();
        
         SetCollidersEnabled(false);
         collider.enabled = true;
         SetRigibodyIsKinematic(true);
    }
    

    private void SetCollidersEnabled(bool enabled)
    {
        foreach (var col in _colliders)
        {
            col.enabled = enabled;
        }
    }

    private void SetRigibodyIsKinematic(bool kinematic)
    {
        foreach (var rig in _rigidbodies)
        {
            rig.isKinematic = kinematic;
        }
    }

    public void AddRagdoll()
    {
        animator.enabled = false;
        SetCollidersEnabled(true);
        SetRigibodyIsKinematic(false);
        collider.enabled = false;
    }
}
