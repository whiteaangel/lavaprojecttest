﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class CameraController : MonoBehaviour
{
   [SerializeField] private Transform movingPoint;
   [SerializeField] private Transform shootPoint;
   [SerializeField] Vector3 cameraOffset;

   [Range(0.01f, 1.0f)] public float smoothFactor = 0.1f;
   public bool lookAtPlayer = false;
   public bool rotateAroundPlayer = true;
   public float rotationSpeed = 5.0f;

   private Transform camTransform;

   private void Awake()
   {
     ChangePointToMove();
      
      var position = camTransform.position;
      transform.position = new Vector3(position.x, 
         position.y + cameraOffset.y, 
         position.z + cameraOffset.z);
   }

   public void ChangePointToShoot()
   {
      camTransform = shootPoint;
   }

   public void ChangePointToMove()
   {
      camTransform = movingPoint;
   }
   
   private void LateUpdate()
   {
      if (rotateAroundPlayer && Input.GetMouseButton(1))
      {
         Quaternion angle = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * rotationSpeed, Vector3.up);
         cameraOffset = angle * cameraOffset;
      }
      var newPos = camTransform.position + cameraOffset;
      transform.position = Vector3.Slerp(transform.position, newPos, smoothFactor);

      if (lookAtPlayer || rotateAroundPlayer)
      {
         transform.LookAt(camTransform);
      }
   }
}
   
