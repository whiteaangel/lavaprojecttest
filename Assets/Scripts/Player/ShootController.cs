﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ShootController : MonoBehaviour
{
    [SerializeField] private Camera camera;
    [SerializeField] private Transform firePoint;
    [SerializeField] private GameObject bullet;
    [SerializeField] private FireRate fireRateObj;
    [SerializeField] private ParticleSystem particleSystem;
    [SerializeField] private LayerMask enemyLayer;
    [SerializeField] private float shootRadius =  7f;
    
    private float _timer;
    
    public bool IsCanShoot()
    {
        var hitEnemies = Physics.OverlapSphere(gameObject.transform.position, shootRadius, enemyLayer);
        if(!(hitEnemies.Length != 0 && hitEnemies.Any()))
            camera.GetComponent<CameraController>().ChangePointToMove();
        return hitEnemies.Length != 0 && hitEnemies.Any();
    }
    
    
    public void Shoot()
    {
        _timer += Time.deltaTime;
        if (!(_timer >= fireRateObj.fireRate)) return;
        if (!Input.GetButton("Fire1")) return;
        _timer = 0f;
        const float distance = 10f;

        var position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
        position = camera.ScreenToWorldPoint(position);

        particleSystem.Play();
        var go =
            Instantiate(bullet, firePoint.position + new Vector3(0, 0, 0.2f), 
                Quaternion.identity);
        go.transform.LookAt(position);
        go.GetComponent<Rigidbody>().AddForce(go.transform.forward * 1000);
    }
    
}
