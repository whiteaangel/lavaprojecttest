﻿using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private Camera camera;
    [SerializeField] private ShootController shootController;
    [SerializeField] private LayerMask runningLayer;
    [SerializeField] private PlayerSpeed playerSpeed;

    private Animator _animator;
    private NavMeshAgent _agent;
    
    private float _dist;
    private bool _isMoved = false;
    private bool _isCanShoot = false;
    private void Start()
    {
        _agent = GetComponent<NavMeshAgent>();
        _animator = GetComponent<Animator>();
        _agent.speed = playerSpeed.speed;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("ShootingPosition")) 
            return;
        _isCanShoot = shootController.IsCanShoot();
    }

    private void Update()
    {
        if (_isCanShoot)
        {
            _animator.SetInteger("AnimState",1);
            camera.GetComponent<CameraController>().ChangePointToShoot();
            shootController.Shoot();
            _isCanShoot =  shootController.IsCanShoot();
            return;
        }

        if (_isMoved && !float.IsPositiveInfinity(_dist) &&
            _agent.pathStatus == NavMeshPathStatus.PathComplete &&
            _agent.remainingDistance == 0)
        {
            _isMoved = false;
            _animator.SetInteger("AnimState",0);
        }
            
        if (!Input.GetMouseButtonDown(0)) return;
        
        var mayRay = camera.ScreenPointToRay(Input.mousePosition);
        
        if (!Physics.Raycast(mayRay, out var hitInfo, 100, runningLayer)) return;
        
        _agent.SetDestination(hitInfo.point);
        _animator.SetInteger("AnimState",2);
        
        _dist = _agent.remainingDistance; 
        _isMoved = true;
    }

    private void LateUpdate()
    {
        if(!_isCanShoot)
            return;
        RotatePlayer();
    }
    
    private void RotatePlayer()
    {
        Ray cameraRay = camera.ScreenPointToRay(Input.mousePosition);
        Plane groundPlane = new Plane(Vector3.up, Vector3.zero);

        if (!groundPlane.Raycast(cameraRay, out var rayLength)) return;
        Vector3 pointToLook = cameraRay.GetPoint(rayLength);

        transform.LookAt(new Vector3(pointToLook.x, transform.position.y, pointToLook.z));
    }
    
}
